# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 20:25:06 2018

@author: Manuel Avila ; Lucas Pavez
"""

#import chance
import json
import time
from random import randint
import os.path as path

def abrirJson ():
	labJson = {}
	if path.exists('laberintos.json'):
		with open('laberintos.json',"r") as file: 
			labJson = json.load(file)
	else:
		print ("No hay un archivo Json con ensenanzas.\n\n\n")
		time.sleep(2)

	return labJson

def escribirEnJson (movimientos):
	
	laberintoJson = abrirJson()
	
	idLab = len(laberintoJson)
	idLaberinto = 'Laberinto'+str(idLab)
	laberintoJson[idLaberinto]=[]
	# append a new lear to laberythm
	nueva_ensenanza = len(laberintoJson[idLaberinto])
	laberintoJson[idLaberinto].append({
		'ensenanza'+str(nueva_ensenanza): movimientos
		})
	
	print ("")
	print ("")
	print ("Se ha agregado exitosamente la ensenanza", nueva_ensenanza, "al laberinto", idLaberinto, "al archivo Json.")
	print ("")
	print ("")

	### TRABAJANDO CON JSON
	with open('laberintos.json',"w") as file:
		json.dump (laberintoJson, file, indent = 4)


if __name__ == "__main__":
	abrirJson()
	juego=1
	while juego == 1:
		movimientos = []
		print (" /// Bienvenido al Laberinto_ICB//")
		print ("")
		print ("[1] = PARED")
		print ("[0] = ESPACIO LIBRE")
		print ("[2] = RUTA RECORRIDA")
		print ("")	
		#dimensions of the matrix
		
		largo = int(input("Ingrese largo de la matriz: "))
		ancho = int(input("Ingrese ancho de la matriz: "))
		
		#create a matrix with 1 and 0 at random
		m = []
		for i in range(largo):
			l=[]
			for j in range(ancho):
				azar = randint(0,100)
				if azar>=0 and azar<=30: 
					l+=[1]
				else:
					l+=[0]			
			m+=[l]
			
		#print matrix
		print "Matriz al azar"
		for i in range(largo):
			print m[i]

		print "__________________________________________"
		print "Matriz modificada"
		
		#just assign a zero in the first row
		ceros = 0
		for i in range(ancho):
			if m[0][i]==0 and ceros==0: #here find the first zero from left to right
				py = i
				px = 0
				ceros+=1 #keep the zero that you found
				
				
			else:
				m[0][i] = 1 #if it says that there is already a zero before, it replaces it with a 1
				
				
			if i==ancho-1 and ceros==0: # assign the last number with a zero if there were only 1
				m[0][i]=0
				py = 0
				px = i


		rx = px
		ry = py #they are backups
		m[px+1][py]=0 #zero below the first
		#we repeat the previous process but with the last row
		ceros = 0
		
		for i in range(ancho):
			if m[ancho-1][i]==0 and ceros<=1: #here find the first zero from left to right
				ceros+=1 #keep the zero that you found
				

			else:
				m[ancho-1][i]=1 #if it says that there is already a zero before, it replaces it with one

				
			if i==ancho-1 and ceros==0: # assign the last number with a zero if there were only 1
				m[ancho-1][i]=0


		print ("") 
		m2 = m # we create a backup matrix

		for i in range(largo):
			print m[i]


		print ""
		rutas=[]
		#movements within the matrix
		posiciones = [[px,py]]
		limite = largo*ancho
		unos = 0
		a=0 #limit interactions
		# 0 which can be moved
		# 1 wall
		# 2 that has already moved
		saltos=0
		#we create everything important backup
		px2 = rx
		ax = px
		py2 = ry
		ay = py
		rutas2=[]
		posiciones2=[]
		m2 = m
		am = m
		unos2 = 0
		saltos2 = 0
		a2 = 0
		print "SU POSICION INICIAL ES :", ax , ay
		am[ax][ay]=3
		for i in range(largo):
			print am[i]

		
		#we give the instructions to the player
		print ""
		print "POSICIONES:"
		print "1 = Izquierda"
		print "2 = Derecha"
		print "3 = Arriba"
		print "4 = Abajo"
		print "0 = CAMINO RESUELTO"
		
		am[ax][ay]=3
		contador=0 
		#while in the last row there is not a 2 will continue playing
		while am[largo-1][ay]!=2:	
			#conditions for mobility
			tecla = int(input("ingrese su desplazamiento:  "))
			print ("")
			
			if tecla == 1 and am[ax][ay-1]==0:
				am[ax][ay-1]=3
				am[ax][ay]=0
				ay = ay-1
				contador=contador+1
				movimientos.append('I')

						
			if tecla==4 and am[ax+1][ay] ==0: 
				am[ax][ay]=0
				am[ax+1][ay]=3
				ax = ax+1
				contador=contador+1
				movimientos.append("A")
				
			if tecla==2 and am[ax][ay+1]==0:
				am[ax][ay+1]=3
				am[ax][ay]=0
				ay = ay+1
				contador=contador+1
				movimientos.append("D")
				
				
			if tecla == 3 and am[ax-1][ay]==0:
				am[ax][ay]=0
				am[ax-1][ay]=3
				ax = ax-1
				contador=contador+1
				movimientos.append("S")
				
				
			for i in range(ancho):
				print am[i]
				
			#if you enter 0 you enter the option of the random solution	
			if tecla == 0:
				primerasolucion=0
				am2 = am
				m[px2][py2]=2
				
				
				while py<largo:# it has to come down				
					posibles = 3 # let's compare the possible paths
					bloqueos = 0 # With how many roads are there
					a=a+1
					aux = 0 # check if there is exit from the position that is


					print "px ", px
					print "py ", py
				

					if px+1<=largo-1: # ask if you can move for the down         
						if aux==0 and m[px+1][py]==0 or m[px+1][py]==3 : 
							m[px+1][py]=2
							px=px+1
							posiciones+=[[px,py]]
							aux= 1            
							saltos+=1
							
							
						else:
							bloqueos+=1
							
							
					else:
						posibles-=1
						

					if py+1<=ancho-1: # ask if you can move to right
						if aux==0 and m[px][py+1]==0 or m[px][py+1]==3:
							m[px][py+1]=2
							py=py+1
							posiciones+=[[px,py]]
							aux = 1
							saltos+=1
							
						else:
							bloqueos+=1
							
							
					else:
						posibles-=1
					
					if py-1>=0: # ask if you can move to the left          
						if aux==0 and m[px][py-1]==0 or m[px][py-1]==3: 
							m[px][py-1]=2
							py=py-1
							posiciones+=[[px,py]]
							aux= 1
							saltos+=1
							
							
						else:
							bloqueos+=1
							
							
					else:
						posibles-=1
						
						
					if bloqueos==posibles:
						
					
						print "Camino encerrado metodo 1"
						break
						
					if px==largo-1: #it only matters if it is already down           
						print ""
						print "Ciclo acabado con exito metodo 1!"
						print "Saltos: ", saltos
						rutas+=[posiciones]
						primerasolucion=1 
						m[px][py]= 1
						break


					if aux == 0:
						m[px][py]=1 #replace the last position traveled to 1
						unos+=1 # we add a one that we keep to a counter that limits the infinite cycle
						px=rx #we start again with the initial values
						py=ry #position equal initial support
						rutas+=[posiciones]
						posiciones=[[px,py]]
						
						for i in range(largo):
							for j in range(ancho):
								if m[i][j]==2:
									m[i][j]=0
									saltos=0    
						m2[px2][py2]=2

						
						if m[px][py]==1:
							print "No existe salida por metodo 1!"
							break  

					if unos>=limite:
						print "Limite de iteraciones metodo 1!"
						break        
					
					print ""    
					for i in range(len(m)):
						print m[i]
						
					print ""
					print posiciones
					
					if a==2000:
						break
					
				print ""

				print "*************************************************************************"

				a2=0

				print ""
				for i in range(largo):
					for j in range(ancho):
						if m2[i][j]==2:
							m2[i][j]=0

				print "matriz del metodo 2"
				for j in range(largo):
					print m2[j]

				print ""
				segundasolucion=0

				m2[px2][py2]=2


				print ""

				while py2<largo:#it only matters that I get down

					posibles = 3 # let's compare the possible paths
					bloqueos = 0 # With how many roads are there
					a2=a2+1
					aux = 0 # check if there is exit from the position that is


					if px2+1<=largo-1: # ask if you can move for the down           
						if aux==0 and m2[px2+1][py2]==0 or m2[px2+1][py2]==3 : 
							m2[px2+1][py2]=2
							px2=px2+1
							posiciones2+=[[px2,py2]]
							aux= 1
							saltos2+=1
							
						else:
							bloqueos+=1
							
					else:
						posibles-=1
						
					if py2-1>=0: # ask if you can move to the left          
						if aux==0 and m2[px2][py2-1]==0 or m2[px2][py2-1]==3: 
							m2[px2][py2-1]=2
							py2=py2-1
							posiciones2+=[[px2,py2]]
							aux= 1
							saltos2+=1
							
						else:
							bloqueos+=1
							
					else:
						posibles-=1
				 
					if py2+1<=ancho-1: # ask if you can move to right
						if aux==0 and m2[px2][py2+1]==0 or m2[px2][py2+1]==3: 
							m2[px2][py2+1]=2
							py2=py2+1
							posiciones2+=[[px2,py2]]
							aux = 1
							saltos2+=1
							
						else:
							bloqueos+=1
							
					else:
						posibles-=1

					if bloqueos==posibles:
						print "Camino encerrado metodo 2"
						break
						
					if px2==largo-1:#check if I finish the cycle

						print ""
						print "Ciclo acabado con exito metodo 2!"
						print "Saltos: ", saltos2
						rutas2+=[posiciones2]
						segundasolucion=1
						break

					if aux == 0:
						m2[px2][py2]=1 #we replace the last position traveled to 1
						unos2+=1 # we add a one that we keep to a counter that limits the infinite cycle
						px2=rx #volvemos a empezar con los valores iniciales
						py2=ry #posicion igual respaldo inicial
						rutas2+=[posiciones2]
						posiciones2=[[px2,py2]]
						
						
						for i in range(largo):
							for j in range(ancho):
								if m2[i][j]==2:
									m2[i][j]=0
									saltos2=0
						
						m2[px2][py2]=2 
						
						
						if m2[px2][py2]==1:
				
							print "No existe salida por metodo 2!"
							break  

					if unos2>=limite:
						print "Limite de iteraciones metodo 2!"
						break        
					
					print ""    
					for i in range(len(m2)):
						print m2[i]
						
					print ""
					print posiciones2
					
					if a2==2000:
						break
					
				print ""
				print ""
				print ""
				print ""
				print "*************************************************************************"



				if primerasolucion==1 and segundasolucion==0:
					# things to print
					print "Se utiliza el metodo 1"    
					print ""
					
					for i in range (len(rutas)):
						
						print "rutas ", i+1
						print rutas[i]
						print ""
					
					if len(posiciones)<=2:
						print "Sin solucion"
						
					else:
						for i in range (len(posiciones)):
							print "Posiciones recorridas finales: ", posiciones[i]
						print ""
						print "saltos finales: ", saltos    
					
					
				if primerasolucion==0 and segundasolucion==1:
					# cosas para imprimir
					print "Se utiliza el metodo 2"    
					print ""

					for i in range (len(rutas2)):
						print "rutas ", i+1
						print rutas2[i]
						print ""
					
					if len(posiciones2)<=2:
						print "Sin solucion"
						
					else:
						for i in range (len(posiciones2)):
							print "Posiciones recorridas finales: ", posiciones2[i]
						print ""
						print "saltos finales: ", saltos2 
					
					
				if primerasolucion==0 and segundasolucion==0:
					print ""
					print "No hay solucion"
					

				print ""
				print "_________________"
				if primerasolucion==1 and segundasolucion==1:
					if saltos<saltos2:
						saltostotales = saltos
						rutatotal=rutas
						posicionestotal = posiciones
						print ""
						print "es mejor el metodo 1"
						print ""
						
					else:
						saltostotales = saltos2
						rutatotal=rutas2
						posicionestotal = posiciones2    
						print ""
						print "es mejor el metodo 2"
						print ""

					for i in range (len(rutatotal)):
						print "rutas ", i+1
						print rutatotal[i]
						print ""
					
					if len(posicionestotal)<=2:
						print "Sin solucion"
						
						
					else:
						for i in range (len(posicionestotal)):
							print "Posiciones recorridas finales: ", posicionestotal[i]
						print ""
						print "saltos finales: ", saltostotales 
							
				  
					
				for i in range(ancho):
					print am[i]
					
						
				print"" 
				print contador
				print posiciones 
		
		escribirEnJson(movimientos)
		#he wonders if he wants to play again
		juego = int(input("quiere seguir jugando : 1) si 2) no  \n\n"""))

	print("")    
	print("////FIN DEL JUEGO////")

