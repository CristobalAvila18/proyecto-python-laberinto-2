# Proyecto Python Laberinto 2
Autores: Manuel Avila; Lucas Pavez

Laberinto ICB

Tablero :

    0 = Camino libre
    1 = Pared
    3 = Jugador
    2 = Camino Recorrido

Movimientos para ingresar:

    1 = Izquierda
    2 = Derecha
    3 = Arriba
    4 = Abajo
    0 = MUESTRA LAS POSIBLES RUTAS


Este programa, intenta simular el juega del laberinto, el cual consiste en que un jugador tiene que desplazarse por medio de un camino libre
desde la entrada hasta una salida.
El Usuario tiene la libertad de poder controlar a este "jugador" por medio del ingreso de numeros que ira pidiendo el programa a lo largo
del ciclo.
Ademas, el programa cuenta con una funcion la cual consiste, en que desde el inicio hasta el final, encuentra la ruta mas optima para poder llegar hasta la meta.
Este procedimiento se realiza a por medio de 2 metodos:

    el metodo 1, encuentra las posibles rutas, prefiriendo tomar caminos libres hacia abajo prefiriendo la derecha antes que la izquierda.
    el metodo 2, encuentra las posibles rutas, prefiriendo tomar caminos libre hacia abajo prefiriendo la izquierda antes que la derecha.
    
una vez analizado los caminos por ambos metodos, el programa cuenta la cantidad de pasos que tuvo que dar el jugador en cada metodo, para asi determinar cual es el mas eficaz.
tambien muestra las posiciones que tuvo que recorrer para llegar a la salida.

este laberinto tambien cuenta con una base de datos por medio de JSON, el cual ira guardando en un archivo, el laberinto en una enumeracion y los movimientos que hizo 
para llegar a la salida.

ADVERTENCIA 

-Este programa no esta 100% funcionando, consta con distintos errores, entre ellos uno de los mas destacados, es que a veces, al mostrarnos la "ayuda", estos metodos siempre prefieren irse 
abajo del todo, antes que irse a la izquierda o derecha, por ende, muchas veces el laberinto tiene soluciones en las cuales hay que irse por otra ruta que no sea hacia abajo, los metedos diran
que el camino esta encerrado y no mostrara la solucion optima aun cuando el laberinto si las tenga.

-Otra problematica de este codigo, es que al momento en el que nos mostros las posibles rutas para llegar a la salida, no podemos volver a controlar al jugador, desde la posicion en la cual
lo dejamos antes de presionar la tecla "0"
